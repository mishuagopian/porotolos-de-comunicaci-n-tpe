package ar.edu.itba.pdc.tcp.proxy.router;

import java.net.InetSocketAddress;

import ar.edu.itba.pdc.tcp.proxy.XMPPProxyConfiguration;

public class ClientServerRouteService {
	
	private static ClientServerRouteService instance;
	
	private ClientServerRouteService() {
	}
	
	public static ClientServerRouteService getInstance() {
		if (instance == null) {
			instance = new ClientServerRouteService();
		}
		return instance;
	}
	
	public InetSocketAddress getRoute(String JID) {
		Address address = XMPPProxyConfiguration.getInstance().getRouteForUser(JID);
		InetSocketAddress addrSocket = new InetSocketAddress(address.getRoute(), address.getPort());
		return addrSocket.isUnresolved() ? null : addrSocket;
	}

}
