package ar.edu.itba.pdc.tcp.proxy.logs;

public class LogsStrings {
	
	public static String NEW_USER = "A new user has entered the proxy.";
	public static String RECEIVED_AUTH = "Received auth from %s@%s.";
	public static String INVALID_AUTH = "Received an invalid auth from the new user. Aborting.";
	public static String FAKE_TO_SERVER = "Starting authentication for user %s@%s with server %s:%s.";
	public static String SENDING_AUTH = "Sending %s@%s auth to server.";
	public static String RECEIVED_SUCCESS = "Received success from server for user %s@%s authentication.";
	public static String RECEIVED_FAILURE = "Received failure from server for user %s@%s authentication.";
	public static String CONNECTED = "User %s@%s connected to server %s:%s.";
	public static String FAILURE_TO_CLIENT = "Sending client %s@%s the reason of the failure."
			+ "Could not connect to server.";
	public static String HOST_UNRECHEABLE = "Trying to reach an unrecheable server "
			+ "for user %s@%s. Aborting.";
	public static String INVALID_XML = "Invalid xml received from user %s@%s. Aborting.";
	public static String FINISH_CONNECTION = "Closing connection for user %s@%s.";
	
	public static String BLOCKED_MESSAGE = "A message was blocked from user %s to user %s.";
	public static String MESSAGE = "Message from user %s to user %s: %s.";
	public static String MESSAGE_AFTER_L33T = "Message from user %s to user %s after Leet convertion: %s.";
}
