package ar.edu.itba.pdc.tcp.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertieManager {
	private static final Logger log = Logger.getLogger("resumeLogger");

	public static String getPropertie(String propertie) {
		Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("./config.properties");
			properties.load(input);
		} catch (IOException e) {
			log.debug("Can not read input stream");
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.debug("Can not close input stream");
				}
			}
		}
		return properties.getProperty(propertie);
	}
}
