package ar.edu.itba.pdc.tcp.proxy.filters;

public class L33tConversor {
	
	private static L33tConversor instance;
	
	private L33tConversor() {
	}
	
	public static L33tConversor getInstance() {
		if (instance == null) {
			instance = new L33tConversor();
		}
		return instance;
	}
	
	public StringBuffer apply(String characters) {
		StringBuffer stringBuffer = new StringBuffer();
		int length = characters.length();
		for (int i = 0; i < length; i++) {
			switch (characters.charAt(i)) {
				case 'a':
					stringBuffer.append("4");
					break;
				case 'e':
					stringBuffer.append("3");
					break;
				case 'i':
					stringBuffer.append("1");
					break;
				case 'o':
					stringBuffer.append("0");
					break;
				case 'c':
					stringBuffer.append("<");
					break;
				default:
					stringBuffer.append(characters.charAt(i));
					break;
			}
		}
		return stringBuffer;
	}
	
	public static void main(String[] args) {
		System.out.println(L33tConversor.getInstance().apply("sdasdlajdlajsdlajscnmxvccasx312"));
	}
}