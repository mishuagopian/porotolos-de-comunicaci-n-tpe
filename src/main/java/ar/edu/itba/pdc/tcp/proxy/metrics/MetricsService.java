package ar.edu.itba.pdc.tcp.proxy.metrics;

public class MetricsService {

	private long access;
	private long receivedBytes;
	private long transferedBytes;
	private long blockedMessages;
	private static MetricsService instance;
	
	private MetricsService() {
		this.access = 0;
		this.receivedBytes = 0;
		this.transferedBytes = 0;
		this.blockedMessages = 0;
	}
	
	public static MetricsService getInstance() {
		if (instance == null) {
			instance = new MetricsService();
		}
		return instance;
	}
	
	public long getAccess() {
		return access;
	}
	
	public long getReceivedBytes() {
		return receivedBytes;
	}

	public long getTransferedBytes() {
		return transferedBytes;
	}
	
	public long getBlockedMessages() {
		return blockedMessages;
	}
	
	public void addAccess() {
		this.access++;
	}
	
	public void addReceivedBytes(long receivedBytes) {
		this.receivedBytes += receivedBytes;
	}
	
	public void addTransferedBytes(long transferedBytes) {
		this.transferedBytes += transferedBytes;
	}
	
	public void addBlockedMessages() {
		this.blockedMessages++;
	}
}
