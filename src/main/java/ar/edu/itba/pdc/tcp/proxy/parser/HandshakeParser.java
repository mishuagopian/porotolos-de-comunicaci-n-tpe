package ar.edu.itba.pdc.tcp.proxy.parser;

import java.nio.ByteBuffer;

import javax.xml.stream.XMLStreamException;

import ar.edu.itba.pdc.tcp.proxy.connection.Connection;

import com.fasterxml.aalto.AsyncByteBufferFeeder;
import com.fasterxml.aalto.AsyncXMLStreamReader;
import com.fasterxml.aalto.stax.InputFactoryImpl;

public class HandshakeParser {

	private AsyncXMLStreamReader<AsyncByteBufferFeeder> factory;
	private static AsyncByteBufferFeeder parser;
	private ByteBuffer byteBuffer;
	int element;

	public HandshakeParser(ByteBuffer byteBuffer) {
		factory = (new InputFactoryImpl()).createAsyncForByteBuffer();
		parser = factory.getInputFeeder();
		this.byteBuffer = byteBuffer;
	}

	public String process(Connection connectionEntity) throws XMLStreamException {
		boolean first = true;
		do {
			if (!factory.hasNext()) {
				return null;
			}
			element = factory.next();
			switch (element) {
				case AsyncXMLStreamReader.START_DOCUMENT:
					continue;
				case AsyncXMLStreamReader.START_ELEMENT:
					String tagName = factory.getLocalName();
					if (tagName.equals("stream")) {
						for (int i = 0; i < factory.getAttributeCount(); i++) {
							String attributeName = factory.getAttributeLocalName(i);
							if (attributeName.equals("to")) {
								connectionEntity.setServerName(factory.getAttributeValue(i));
							}
						}
					}
					return tagName;
				case AsyncXMLStreamReader.CHARACTERS:
					// Auth case: returning username base64 encoded
					return factory.getText();
				case AsyncXMLStreamReader.EVENT_INCOMPLETE:
					if (first) {
						first = false;
						byteBuffer.flip();
						parser.feedInput(byteBuffer);
						break;
					} else {
						byteBuffer.clear();
						return null;
					}
				case AsyncXMLStreamReader.END_ELEMENT:
					String name = factory.getName().getLocalPart();
					if (name.equals("features")) {
						return name;
					}
					break;	
				default:
					break;
			}
		} while (element != AsyncXMLStreamReader.END_DOCUMENT);
		return null;
	}

	public void close() {
		try {
			factory.close();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

}
