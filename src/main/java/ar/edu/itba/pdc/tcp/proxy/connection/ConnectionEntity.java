package ar.edu.itba.pdc.tcp.proxy.connection;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import ar.edu.itba.pdc.tcp.protocols.TCPProtocol;
import ar.edu.itba.pdc.tcp.proxy.logs.LogsStrings;
import ar.edu.itba.pdc.tcp.proxy.parser.HandshakeParser;
import ar.edu.itba.pdc.tcp.proxy.parser.StanzaParser;

public class ConnectionEntity implements Connection {

	private SocketChannel clientChannel = null;
	private SocketChannel serverChannel = null;

	private String clientUsernameBase64 = null;
	private String clientUsername = null;
	
	private static final String TO_CLIENT_STREAM = "<?xml version='1.0' ?><stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>";
	private static final String TO_CLIENT_NEGOTIATION = "<stream:features><mechanisms xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\"><mechanism>PLAIN</mechanism></mechanisms><auth xmlns=\"http://jabber.org/features/iq-auth\"/></stream:features>";	
	private static final String TO_CLIENT_HOST_UNRECHEABLE = "<failure xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><aborted/></failure>";
	private static final String TO_CLIENT_INVALID_AUTH = "<stream:error><not-authorized xmlns='urn:ietf:params:xml:ns:xmpp-streams'/></stream:error>";
	protected static final String TO_CLIENT_INVALID_XML = "<stream:error><xml-not-well-formed xmlns='urn:ietf:params:xml:ns:xmpp-streams'/></stream:error>";
	private static final String ROLE = "client";
	
	private String serverName;

	private State state;
	
	private static final int BUFSIZE = 1024;
	private ByteBuffer clientRead;		// can be the server...
	private ByteBuffer clientWrite;		// a way to define the user who created the connection
	private ByteBuffer serverRead;
	private ByteBuffer serverWrite;

	private TCPProtocol protocol;
	private HandshakeParser handshakeParser;
	private StanzaParser stanzaParser;
	
	private Boolean handshake;
	
	private Selector selector;
	
	public ConnectionEntity(TCPProtocol protocol, Selector selector) {
		this.protocol = protocol;
		this.selector = selector;
		this.handshake = true;
		this.serverName = null;
		this.state = State.waitingInitialStream;
		this.clientRead = ByteBuffer.allocate(BUFSIZE);
		this.clientWrite = ByteBuffer.allocate(BUFSIZE*4);
		this.serverRead = ByteBuffer.allocate(BUFSIZE);
		this.serverWrite = ByteBuffer.allocate(BUFSIZE*4);
		this.handshakeParser = new HandshakeParser(clientRead);
		this.stanzaParser = new StanzaParser(clientRead, serverWrite, clientWrite, true);
	}
	
	public void process() {
		String[] splitAuth = null;	
		String xml = null;
		try {
			while (!handshake || (xml = handshakeParser.process(this)) != null) {	
				switch (state) {
					case waitingInitialStream:
						if (xml.equals("stream")) {
							state = State.waitingAuth;
							streamAndNegotiationToClient();
							return;
						}
						break;
					case waitingAuth:
						if (xml.equals("auth")) {
							state = State.connectingWithServer;
						}
						break;
					case connectingWithServer:
						clientUsernameBase64 = xml;
						try {
							splitAuth = new String(Base64.decodeBase64(xml),"UTF-8").split("\0");
							if (splitAuth.length < 3) {
								invalidAuth();
								return;
							}
							clientUsername = splitAuth[1];
						} catch (UnsupportedEncodingException e) {
						}
						if (clientUsername != null) {
							state = State.connectionReady;
							clientRead.clear();
							handshakeParser.close();
							handshake = false;
							
							Logger.getLogger("resumeLogger").debug(
									String.format(LogsStrings.RECEIVED_AUTH, clientUsername, getServerName()));
						}
						return;
					case connected:
						stanzaParser.process();
						return;
					default:
						break;
				}
			}
		} catch (XMLStreamException e) {
			invalidXML();
		}
	}
	
	private void invalidAuth() {
		state = State.invalidAuth;
		clientWrite.put(TO_CLIENT_INVALID_AUTH.getBytes());
		
		Logger.getLogger("resumeLogger").debug(LogsStrings.INVALID_AUTH);
	}
	
	private void streamAndNegotiationToClient() {
		clientWrite.put((TO_CLIENT_STREAM).getBytes());
		clientWrite.put((TO_CLIENT_NEGOTIATION).getBytes());
		
		Logger.getLogger("resumeLogger").debug(LogsStrings.NEW_USER);
	}
	
	private void invalidXML() {
		state = State.invalidXML;
		clientWrite.put((TO_CLIENT_INVALID_XML).getBytes());
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.INVALID_XML, getClientUsername(), getServerName()));
	}
	
	public void hostUnreacheable() {
		state = State.hostUnrecheable;
		clientWrite.put((TO_CLIENT_HOST_UNRECHEABLE).getBytes());
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.HOST_UNRECHEABLE, getClientUsername(), getServerName()));
	}
	
	public void finishConnection() {
		finishConnection(clientChannel);
		finishConnection(serverChannel);
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.FINISH_CONNECTION, getClientUsername(), getServerName()));
	}
	
	private void finishConnection(SocketChannel channel) {
		if (channel != null) {
			try {
				channel.close();
			} catch (IOException e) {
			} 
			if (channel.keyFor(selector) != null) {
				channel.keyFor(selector).cancel();
			}
		}
	}
	
	public void loadBuffers(){
		stanzaParser.loadBuffer(BUFSIZE);
	}
	
	public TCPProtocol getProtocol() {
		return protocol;
	}
	
	public ByteBuffer getClientRead() {
		return clientRead;
	}

	public ByteBuffer getClientWrite() {
		return clientWrite;
	}

	public ByteBuffer getServerRead() {
		return serverRead;
	}

	public ByteBuffer getServerWrite() {
		return serverWrite;
	}

	public boolean connectionReady() {
		return state == State.connectionReady;
	}
	
	public boolean connectionFailed() {
		return state == State.connectionFailed;
	}

	public boolean isHostUnrecheable() {
		return state == State.hostUnrecheable;
	}
	
	public boolean isInvalidXML() {
		return state == State.invalidXML;
	}
	
	public boolean isInvalidAuth() {
		return state == State.invalidAuth;
	}
	
	public String getClientUsername() {
		return clientUsername;
	}
	
	public String getClientUsernameBase64() {
		return clientUsernameBase64;
	}

	public SelectionKey getClientChannelKey() {
		if (clientChannel == null) {
			return null;
		}
		return clientChannel.keyFor(selector);
	}

	public SelectionKey getServerChannelKey() {
		if (serverChannel == null) {
			return null;
		}
		return serverChannel.keyFor(selector);
	}
	
	public String getRole() {
		return ROLE;
	}
	
	public void setClientChannel(SocketChannel clientChannel) {
		this.clientChannel = clientChannel;
	}

	public void setServerChannel(SocketChannel serverChannel) {
		this.serverChannel = serverChannel;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public int getBufsize() {
		return BUFSIZE;
	}
	
	public StanzaParser getStanzaParser() {
		return this.stanzaParser;
	}
	
	public String getServerName() {
		return serverName;
	}
	
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
}
