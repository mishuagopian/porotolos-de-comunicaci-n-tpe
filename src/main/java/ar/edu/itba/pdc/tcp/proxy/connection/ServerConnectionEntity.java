package ar.edu.itba.pdc.tcp.proxy.connection;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import org.apache.log4j.Logger;

import ar.edu.itba.pdc.tcp.protocols.TCPProtocol;
import ar.edu.itba.pdc.tcp.proxy.XMPPProxyConfiguration;
import ar.edu.itba.pdc.tcp.proxy.logs.LogsStrings;
import ar.edu.itba.pdc.tcp.proxy.parser.HandshakeParser;
import ar.edu.itba.pdc.tcp.proxy.parser.StanzaParser;
import ar.edu.itba.pdc.tcp.proxy.router.Address;

public class ServerConnectionEntity implements Connection {

	private ConnectionEntity connectionEntity;
	private HandshakeParser handshakeParser;
	private StanzaParser stanzaParser;
	private State state;
	
	private static final String TO_SERVER_STREAM = "<?xml version='1.0' ?><stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0' to = '%s' >";
	private static final String TO_SERVER_AUTH = "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>%s</auth>";
	private static final String FAILURE_ELEMENT = "<failure xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><%s/></failure>";
	
	private static final String ROLE = "server";

	private boolean handshake;
	
	public ServerConnectionEntity(ConnectionEntity connectionEntity) {
		this.connectionEntity = connectionEntity;
		this.handshake = true;
		this.state = State.waitingInitialStream;
		this.handshakeParser = new HandshakeParser(connectionEntity.getServerRead());
		this.stanzaParser = new StanzaParser(connectionEntity.getServerRead(), connectionEntity.getClientWrite(), connectionEntity.getServerWrite(), false);
		this.stanzaParser.setJID(connectionEntity.getClientUsername() + "@" + getServerName());
		this.connectionEntity.getStanzaParser().setJID(connectionEntity.getClientUsername() + "@" + getServerName());
		streamToServer();
	}
	
	public void process() {
		String xml = null;
		try {
			while (!handshake || (xml = handshakeParser.process(this)) != null) {
				switch (state) {
					case waitingInitialStream:
						if (xml.equals("stream")) {
							state = State.waitingStreamFeatures;
						}
						break;
					case waitingStreamFeatures:
						if (xml.equals("features")) {
							state = State.readingStreamFeatures;
							break;
						}
						// Always waiting for stream features
					case readingStreamFeatures:
						if (xml.equals("features")) {
							sendAuth();
							state = State.authConfirmation;
						}
						break;
					case authConfirmation:
						if (xml.equals("success")) {
							successToClient();
							handshake = false;
							handshakeParser.close();
							connectionEntity.setState(State.connected);
							state = State.connected;
							return;
						} else if (xml.equals("failure")) {
							state = State.waitingFailureReason;
							
							Logger.getLogger("resumeLogger").debug(
									String.format(LogsStrings.RECEIVED_FAILURE, getClientUsername(), getServerName()));
						}
						break;
					case waitingFailureReason:
						getServerRead().clear();
						failureToClient(xml);
						state = State.connectionFailed;
						connectionEntity.setState(State.connectionFailed);
						break;
					case connected:
						stanzaParser.process();
						return;
					default:
						break;
				}
			}	
		} catch (Exception e) {
			invalidXML();
		}
	}
	
	public void loadBuffers(){
		stanzaParser.loadBuffer(connectionEntity.getBufsize());
	}
	
	private void invalidXML() {
		state = State.invalidXML;
		connectionEntity.setState(State.invalidXML);
		getClientWrite().put((ConnectionEntity.TO_CLIENT_INVALID_XML).getBytes());
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.INVALID_XML, getClientUsername(), getServerName()));
	}
	
	private void successToClient() {
		getServerWrite().put(getClientRead());
		getClientRead().clear();
		getServerRead().clear();
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.RECEIVED_SUCCESS, getClientUsername(), getServerName()));
		Address address = XMPPProxyConfiguration.getInstance().getRouteForUser(getClientUsername());
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.CONNECTED,
						getClientUsername(), getServerName(),
						address.getRoute(), address.getPort()));
	}
	
	private void failureToClient(String xml) {
		String failure = String.format(FAILURE_ELEMENT, xml);
		getServerWrite().put(failure.getBytes());
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.FAILURE_TO_CLIENT, getClientUsername(), getServerName()));
	}
	
	private void streamToServer() {
		String stream = String.format(TO_SERVER_STREAM, getServerName());
		getClientWrite().put(stream.getBytes());
		
		Address address = XMPPProxyConfiguration.getInstance().getRouteForUser(getClientUsername());
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.FAKE_TO_SERVER,
						getClientUsername(), getServerName(),
						address.getRoute(), address.getPort()));
	}

	private void sendAuth() {
		String auth = String.format(TO_SERVER_AUTH, connectionEntity.getClientUsernameBase64());
		getClientWrite().put(auth.getBytes());
		
		Logger.getLogger("resumeLogger").debug(
				String.format(LogsStrings.SENDING_AUTH, getClientUsername(), getServerName()));
	}
	
	public void finishConnection() {
		connectionEntity.finishConnection();
	}

	public ByteBuffer getClientRead() {
		return connectionEntity.getServerRead();
	}

	public ByteBuffer getClientWrite() {
		return connectionEntity.getServerWrite();
	}

	public ByteBuffer getServerRead() {
		return connectionEntity.getClientRead();
	}

	public ByteBuffer getServerWrite() {
		return connectionEntity.getClientWrite();
	}

	public String getUsername() {
		return connectionEntity.getClientUsername();
	}

	public TCPProtocol getProtocol() {
		return connectionEntity.getProtocol();
	}
	
	public boolean connectionReady() {
		return false;
	}
	
	public boolean connectionFailed() {
		return state == State.connectionFailed;
	}
	
	public boolean isHostUnrecheable() {
		return false;
	}

	public boolean isInvalidXML() {
		return state == State.invalidXML;
	}
	
	public boolean isInvalidAuth() {
		return false;
	}
	
	public void setClientChannel(SocketChannel channel) {
		connectionEntity.setServerChannel(channel);
	}

	public void setServerChannel(SocketChannel channel) {
		connectionEntity.setClientChannel(channel);
	}

	public SelectionKey getClientChannelKey() {
		return connectionEntity.getServerChannelKey();
	}
	
	public SelectionKey getServerChannelKey() {
		return connectionEntity.getClientChannelKey();
	}

	public String getClientUsername() {
		return connectionEntity.getClientUsername();
	}
	
	public String getClientUsernameBase64() {
		return null;
	}
	
	public String getRole() {
		return ROLE;
	}
	
	public String getServerName() {
		return connectionEntity.getServerName();
	}
	
	public void setServerName(String serverName) {
		return;
	}

}
