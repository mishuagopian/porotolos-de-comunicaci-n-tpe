package ar.edu.itba.pdc.tcp.protocols.xmpp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import org.apache.log4j.Logger;

import ar.edu.itba.pdc.tcp.protocols.TCPProtocol;
import ar.edu.itba.pdc.tcp.proxy.connection.Connection;
import ar.edu.itba.pdc.tcp.proxy.connection.ConnectionEntity;
import ar.edu.itba.pdc.tcp.proxy.connection.ServerConnectionEntity;
import ar.edu.itba.pdc.tcp.proxy.metrics.MetricsService;
import ar.edu.itba.pdc.tcp.proxy.router.ClientServerRouteService;

public class XMPPProxySelectorProtocol implements TCPProtocol {

	public XMPPProxySelectorProtocol() {
	}

	public void handleAccept(SelectionKey key) throws IOException {
		SocketChannel clientChannel = acceptClient(key);
		ConnectionEntity connectionEntity = new ConnectionEntity(this, key.selector());
		connectionEntity.setClientChannel(clientChannel);
		clientChannel.register(key.selector(), SelectionKey.OP_READ, connectionEntity);
		MetricsService.getInstance().addAccess();
	}

	private SocketChannel acceptClient(SelectionKey key) throws IOException {
		SocketChannel clientChannel = ((ServerSocketChannel)key.channel()).accept();
		clientChannel.configureBlocking(false);
		return clientChannel;
	}

	public void handleRead(SelectionKey key) throws IOException {
		SocketChannel clientChannel = (SocketChannel)key.channel();
		Connection connectionEntity = (Connection)key.attachment();
		try {		
			if (clientChannel.isOpen()) {
				long bytesRead = clientChannel.read(connectionEntity.getClientRead());
				if (bytesRead == -1) {
					clientChannel.close();
				} else if (bytesRead > 0) { 
					
					Logger.getLogger("plainLogger").debug(
							(connectionEntity.getClientUsername() == null ? "unknown" : connectionEntity.getClientUsername() 
								+ "@" + connectionEntity.getServerName())
								+ " reads from " + connectionEntity.getRole()+": "
								+ new String(connectionEntity.getClientRead().array()).substring(0, connectionEntity.getClientRead().position()));
					
					MetricsService.getInstance().addReceivedBytes(bytesRead);
					connectionEntity.process();
					connectionEntity.loadBuffers();
					if (connectionEntity.connectionReady()) {
						establishClientServerConnection(connectionEntity, key.selector());
					}
					if (connectionEntity.getClientChannelKey() != null && connectionEntity.getClientChannelKey().isValid() && connectionEntity.getClientChannelKey().channel().isOpen()) {		
						connectionEntity.getClientChannelKey().interestOps(connectionEntity.getClientWrite().position() > 0 ? SelectionKey.OP_READ | SelectionKey.OP_WRITE : SelectionKey.OP_READ);
					}
					if (connectionEntity.getServerChannelKey() != null && connectionEntity.getServerChannelKey().isValid() && connectionEntity.getServerChannelKey().channel().isOpen()) {
						connectionEntity.getServerChannelKey().interestOps(connectionEntity.getServerWrite().position() > 0 ? SelectionKey.OP_READ | SelectionKey.OP_WRITE : SelectionKey.OP_READ);
					}
				}	
			} else {
				connectionEntity.finishConnection();
			}
		} catch (IOException e) {
			connectionEntity.finishConnection();
		}
	}

	public void handleWrite(SelectionKey key) throws IOException {
		SocketChannel clientChannel = (SocketChannel)key.channel();
		Connection connectionEntity = (Connection)key.attachment();
		try {
			ByteBuffer buffer = connectionEntity.getClientWrite();
			long writtenBytes;
			if (clientChannel.isOpen()) {
				buffer.flip();

				Logger.getLogger("plainLogger").debug(
						(connectionEntity.getClientUsername() == null ? "unknown" : connectionEntity.getClientUsername() 
								+ "@" + connectionEntity.getServerName())
						+ " writes to " 
						+ connectionEntity.getRole()
						+ ": " + new String(buffer.array()).substring(0, buffer.limit()));
				
				writtenBytes = clientChannel.write(buffer);
				buffer.compact();
				MetricsService.getInstance().addTransferedBytes(writtenBytes);
				connectionEntity.loadBuffers();
				if ((connectionEntity.connectionFailed() && buffer.hasRemaining()) 
						|| connectionEntity.isHostUnrecheable()
						|| connectionEntity.isInvalidXML()
						|| connectionEntity.isInvalidAuth()) {
					connectionEntity.finishConnection();
				}
				if (connectionEntity.getClientChannelKey() != null && connectionEntity.getClientChannelKey().isValid() && connectionEntity.getClientChannelKey().channel().isOpen()) {		
					connectionEntity.getClientChannelKey().interestOps(connectionEntity.getClientWrite().position() > 0 ? SelectionKey.OP_READ | SelectionKey.OP_WRITE : SelectionKey.OP_READ);
				}
				if (connectionEntity.getServerChannelKey() != null && connectionEntity.getClientChannelKey().isValid() && connectionEntity.getServerChannelKey().channel().isOpen()) {
					connectionEntity.getServerChannelKey().interestOps(connectionEntity.getServerWrite().position() > 0 ? SelectionKey.OP_READ | SelectionKey.OP_WRITE : SelectionKey.OP_READ);
				}
			} else {
				connectionEntity.finishConnection();
			}		
		} catch (Exception e) {
			connectionEntity.finishConnection();
		}
	}
	
	private void establishClientServerConnection(Connection connectionEntity, Selector selector)
			throws IOException {
		ClientServerRouteService clientServerRouteService = ClientServerRouteService.getInstance();
		InetSocketAddress serverAddress = clientServerRouteService.getRoute(connectionEntity.getClientUsername()
															+ "@" + connectionEntity.getServerName());
		if (serverAddress != null) {
			SocketChannel serverChannel = SocketChannel.open();
			serverChannel.connect(serverAddress);
			serverChannel.configureBlocking(false);
			ServerConnectionEntity serverConnectionEntity = new ServerConnectionEntity((ConnectionEntity)connectionEntity);
			int selectionKey = serverChannel.finishConnect() ? SelectionKey.OP_WRITE : SelectionKey.OP_CONNECT;
			serverConnectionEntity.setClientChannel(serverChannel);
			serverChannel.register(selector, selectionKey, serverConnectionEntity);
		} else {
			((ConnectionEntity)connectionEntity).hostUnreacheable();
		}
	}

}