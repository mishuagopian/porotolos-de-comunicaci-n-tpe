package ar.edu.itba.pdc.tcp.proxy.filters;

import java.util.HashMap;
import java.util.Map;

public class XMLBodyEncoder {
	
	private static XMLBodyEncoder instance;
	private Map<Character,String> mapping;
	
	private XMLBodyEncoder() {
		mapping = new HashMap<Character,String>();
		mapping.put('"', "&quot;");
		mapping.put('\'', "&apos;");
		mapping.put('<', "&lt;");
		mapping.put('>', "&gt;");
		mapping.put('&', "&amp;");
	
	}
	
	public static XMLBodyEncoder getInstance(){
		if(instance == null){
			instance = new XMLBodyEncoder();
		}
		return instance;
	}
	
	public StringBuffer encode(String characters){
		StringBuffer stringBuffer = new StringBuffer();
		int length = characters.length();
		for (int i = 0; i < length; i++) {
			Character c = characters.charAt(i);
			switch (c) {
				case '"':
				case '\'':
				case '<':
				case '>':
				case '&':
					stringBuffer.append(mapping.get(c));
					break;
				default:
					stringBuffer.append(c);
					break;
			}
		}
		return stringBuffer;
	}
	
}
