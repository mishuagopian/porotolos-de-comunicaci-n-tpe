package ar.edu.itba.pdc.tcp.proxy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ar.edu.itba.pdc.tcp.proxy.router.Address;

public class XMPPProxyConfiguration {

	private Map<String, Address> clientServerRoute;
	private boolean l33tConverter;
	private Set<String> silencedUsers;
	private Address defaultRoute;
	private static XMPPProxyConfiguration instance;
	
	private XMPPProxyConfiguration() {
		this.clientServerRoute = new HashMap<String, Address>();
		this.silencedUsers = new HashSet<String>();
		this.defaultRoute = new Address("localhost", "5222");
	}
	
	public static XMPPProxyConfiguration getInstance() {
		if (instance == null) {
			instance = new XMPPProxyConfiguration();
		}
		return instance;
	}
	
	public boolean muteUser(String user) {
		if (!silencedUsers.add(user) && !silencedUsers.contains(user)) {
			return false;
		}
		return true;
	}
	
	public boolean unmuteUser(String user) {
		return silencedUsers.remove(user);
	}
	
	public boolean isSilenced(String user) {
		return silencedUsers.contains(user);
	}
	
	public boolean isL33tConverterActive() {
		return l33tConverter;
	}
	
	public void setL33tConverter(boolean l33tConverter) {
		this.l33tConverter = l33tConverter;
	}
	
	public void setDefaultRoute(String route, String port) {
		this.defaultRoute = new Address(route, port);
	}
	
	public void addSpecificRouteForUser(String user, String route, String port) {
		Address address = new Address(route, port);
		clientServerRoute.put(user, address);
	}
	
	public void deleteSpecificRouteForUser(String user) {
		clientServerRoute.remove(user);
	}
	
	public Address getRouteForUser(String user) {
		Address address = clientServerRoute.get(user);
		return address != null ? address : defaultRoute;
	}
	
}
