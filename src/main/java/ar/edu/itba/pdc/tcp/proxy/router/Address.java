package ar.edu.itba.pdc.tcp.proxy.router;

public class Address {

	private String route;
	private int port;
	
	public Address(String route, String port) {
		this.route = route;
		this.port = Integer.valueOf(port);
	}
	
	public int getPort() {
		return port;
	}

	public String getRoute() {
		return route;
	}
	
}
