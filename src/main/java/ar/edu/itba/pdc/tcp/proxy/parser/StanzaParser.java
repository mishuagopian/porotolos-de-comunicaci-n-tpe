package ar.edu.itba.pdc.tcp.proxy.parser;

import java.nio.ByteBuffer;
import java.util.Stack;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;

import ar.edu.itba.pdc.tcp.proxy.XMPPProxyConfiguration;
import ar.edu.itba.pdc.tcp.proxy.filters.L33tConversor;
import ar.edu.itba.pdc.tcp.proxy.filters.XMLBodyEncoder;
import ar.edu.itba.pdc.tcp.proxy.logs.LogsStrings;
import ar.edu.itba.pdc.tcp.proxy.metrics.MetricsService;

import com.fasterxml.aalto.AsyncByteBufferFeeder;
import com.fasterxml.aalto.AsyncXMLStreamReader;
import com.fasterxml.aalto.stax.InputFactoryImpl;

public class StanzaParser {

	private String MESSAGE_REJECTED_STANZA = "<message id=\"%s\" from=\"%s\" to=\"%s\" type=\"error\"><error "
			+ "type=\"cancel\"><not-allowed "
			+ "xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\"/></error></message>";

	private AsyncXMLStreamReader<AsyncByteBufferFeeder> factory;
	private AsyncByteBufferFeeder parser;
	private ByteBuffer readBuffer;
	private ByteBuffer writeBuffer;
	private ByteBuffer errorBuffer;
	private Stack<String> actualTagName;
	private boolean writesOnServer;
	boolean transmitError;
	private boolean isRejecting;
	private int insideBody;
	private String JID;
	private String from;
	private String to;
	private String id;
	private StringBuffer pendingInformation;
	private StringBuffer pendingInformationError;

	public StanzaParser(ByteBuffer readBuffer, ByteBuffer writeBuffer,
			ByteBuffer errorBuffer, boolean writesOnServer) {
		this.factory = (new InputFactoryImpl()).createAsyncForByteBuffer();
		this.parser = factory.getInputFeeder();
		this.readBuffer = readBuffer;
		this.writeBuffer = writeBuffer;
		this.errorBuffer = errorBuffer;
		this.actualTagName = new Stack<String>();
		this.writesOnServer = writesOnServer;
		this.pendingInformation = new StringBuffer();
		this.pendingInformationError = new StringBuffer();
		this.isRejecting = false;
		this.transmitError = false;
		this.insideBody = 0;
		this.JID = null;
		this.from = null;
		this.to = null;
		this.id = null;
	}

	public void process() throws XMLStreamException {
		int element;
		StringBuffer xmlString = new StringBuffer();
		readBuffer.flip();
		parser.feedInput(readBuffer);
		readBuffer.clear();
	
		do {
			if (!factory.hasNext()) {
				return;
			}
			element = factory.next();
			switch (element) {
			case AsyncXMLStreamReader.START_DOCUMENT:
				continue;
			case AsyncXMLStreamReader.START_ELEMENT:
				String tagName = factory.getPrefixedName();
				actualTagName.push(tagName);
				if (isRejecting){
					break;
				}
				if (tagName.equals("body")) {
					insideBody++;
				} else if (tagName.equals("message")) {
					from = null;
					to = null;
					id = null;
				}
				xmlString.append("<").append(tagName);
				for (int i = 0; i < factory.getAttributeCount(); i++) {
					String attributeName = factory.getAttributeLocalName(i);
					String attributeValue = factory.getAttributeValue(i);
					from = attributeName.equals("from") ? attributeValue : from;
					to = attributeName.equals("to") ? attributeValue : to;
					id = attributeName.equals("id") ? attributeValue : id;
					xmlString.append(" ").append(attributeName)
							.append("='").append(attributeValue)
							.append("'");
					if (tagName.equals("message")
							&& (attributeName.equals("from") || attributeName.equals("to"))) {
						if (XMPPProxyConfiguration.getInstance().isSilenced(attributeValue.split("/")[0])) {
							isRejecting = true;
						}
					}
				}
				if (tagName.equals("message") && XMPPProxyConfiguration.getInstance().isSilenced(JID)) {
					isRejecting = true;
				}
				for (int i = 0; i < factory.getNamespaceCount(); i++) {
					String namespacePrefix = factory.getNamespacePrefix(i);
					String namespacePrefixURI = factory.getNamespaceURI(i);
					xmlString.append(" xmlns")
							.append(((namespacePrefix.isEmpty()) ? "" : ":"))
							.append(namespacePrefix).append("=\"")
							.append(namespacePrefixURI).append("\"");
				}
				xmlString.append(">");
				break;
			case AsyncXMLStreamReader.CHARACTERS:
				String characters = factory.getText();
				if (isRejecting) {
					break;
				}
				if (insideBody > 0) {
					Logger.getLogger("resumeLogger").debug(
						String.format(LogsStrings.MESSAGE, 
								(from == null ? JID : from).split("/")[0], to.split("/")[0], characters));
					if (XMPPProxyConfiguration.getInstance().isL33tConverterActive()) {
						characters = L33tConversor.getInstance().apply(characters).toString();
							Logger.getLogger("resumeLogger").debug(
								String.format(LogsStrings.MESSAGE_AFTER_L33T,
										(from == null ? JID : from).split("/")[0], to.split("/")[0], characters));								
						}
				}
				characters = XMLBodyEncoder.getInstance().encode(characters).toString();
				xmlString.append(characters);
				break;
			case AsyncXMLStreamReader.END_ELEMENT:
				String endTagName = factory.getPrefixedName();
				actualTagName.pop();
				if (endTagName.equals("body")) {
					insideBody--;
				}
				if (isRejecting && endTagName.equals("message")) {
					MetricsService.getInstance().addBlockedMessages();
					isRejecting = false;
					if(from == null){
						from = JID;
					}
					xmlString = to == null || from == null || id == null ? 
										new StringBuffer() : new StringBuffer(String.format(MESSAGE_REJECTED_STANZA, id, from, to));
					
					Logger.getLogger("resumeLogger").debug(
							String.format(LogsStrings.BLOCKED_MESSAGE, from.split("/")[0], to.split("/")[0]));					
										
					if (writesOnServer) {
						transmitError = true;
						pendingInformation = new StringBuffer();
						pendingInformationError.append(xmlString.toString());
					}
					xmlString = new StringBuffer();
				} else {
					xmlString.append("</").append(endTagName).append(">");
				}
				break;
			case AsyncXMLStreamReader.EVENT_INCOMPLETE:
				readBuffer.clear();
				return;
			default:
				break;
			}
			if (xmlString != null && !isRejecting) {
				String xmlStringReady = xmlString.toString();
				if (xmlStringReady != null) {
					pendingInformation.append(xmlStringReady.toString());
				}
				xmlString = new StringBuffer();
			}
		} while (element != AsyncXMLStreamReader.END_DOCUMENT);
	}
	
	public void loadBuffer(int size){
		int sent = 0;
		if(transmitError){
			sent = pendingInformationError.toString().length() > size ? size : pendingInformationError.toString().length();
			errorBuffer.put(pendingInformationError.toString().substring(0, sent).getBytes());
			pendingInformationError = pendingInformationError.delete(0, sent);
			transmitError = sent < size ? false : true;
		} else {
			sent = pendingInformation.toString().length() > size ? size : pendingInformation.toString().length();
			writeBuffer.put(pendingInformation.toString().substring(0, sent).getBytes());
			pendingInformation = pendingInformation.delete(0, sent);
		}		
	}

	public void setJID(String JID) {
		this.JID = JID;
	}
	
}
