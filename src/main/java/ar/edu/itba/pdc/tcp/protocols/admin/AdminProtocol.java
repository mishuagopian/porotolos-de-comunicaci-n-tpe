package ar.edu.itba.pdc.tcp.protocols.admin;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import ar.edu.itba.pdc.tcp.protocols.TCPProtocol;
import ar.edu.itba.pdc.tcp.protocols.admin.parser.AdminParser;

public class AdminProtocol implements TCPProtocol {

	@Override
	public void handleAccept(SelectionKey key) throws IOException {
		SocketChannel clientChannel = acceptChannel(key);
		AdminParser adminParser = new AdminParser(clientChannel);
		clientChannel.register(key.selector(), SelectionKey.OP_READ, adminParser);
	}

	private SocketChannel acceptChannel(SelectionKey key) throws IOException {
		SocketChannel clientChannel = ((ServerSocketChannel) key.channel()).accept();
		clientChannel.configureBlocking(false);
		return clientChannel;
	}

	@Override
	public void handleRead(SelectionKey key) throws IOException {
		SocketChannel clientChannel = (SocketChannel) key.channel();
		AdminParser adminParser = (AdminParser) key.attachment();
		if (clientChannel.isOpen()) {
			long bytesRead = adminParser.read();
			if (bytesRead == -1) {
				clientChannel.close();
			} else if (bytesRead > 0) {
				if (adminParser.isLineDone()) {
					key.interestOps(SelectionKey.OP_WRITE);
				} else {
					key.interestOps(SelectionKey.OP_READ);
				}
			}
		} else {
			key.cancel();
		}
	}

	@Override
	public void handleWrite(SelectionKey key) throws IOException {
		SocketChannel clientChannel = (SocketChannel) key.channel();
		AdminParser adminParser = (AdminParser) key.attachment();
		if (clientChannel.isOpen()) {
			ByteBuffer serverBuffer = adminParser.getServerBuffer();
			serverBuffer.flip();
			clientChannel.write(serverBuffer);
			if (!adminParser.isConnected()) {
				clientChannel.close();
				key.cancel();
				return;
			}
			if (!serverBuffer.hasRemaining()) {
				key.interestOps(SelectionKey.OP_READ);
			}
			serverBuffer.compact();
		} else {
			key.cancel();
		}
	}
}
