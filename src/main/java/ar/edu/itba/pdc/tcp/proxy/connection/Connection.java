package ar.edu.itba.pdc.tcp.proxy.connection;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public interface Connection {

	public ByteBuffer getClientWrite();
	
	public ByteBuffer getClientRead();
	
	public ByteBuffer getServerWrite();
	
	public ByteBuffer getServerRead();
	
	public String getClientUsername();
	
	public String getClientUsernameBase64();
	
	public void process();
	
	public boolean connectionReady();

	public boolean connectionFailed();
	
	public boolean isHostUnrecheable();
	
	public boolean isInvalidXML();
	
	public boolean isInvalidAuth();
	
	public void finishConnection();
	
	public SelectionKey getClientChannelKey();
	
	public SelectionKey getServerChannelKey();
	
	public String getRole();
	
	public String getServerName();
	
	public void setServerName(String serverName);
	
	public void loadBuffers();
	
}
