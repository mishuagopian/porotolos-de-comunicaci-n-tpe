package ar.edu.itba.pdc.tcp.proxy.connection;

public enum State {
	
	waitingInitialStream,
	waitingAuth,
	connectingWithServer,
	success,
	connectionReady,
	waitingStreamFeatures,
	readingStreamFeatures,
	authConfirmation,
	connected,
	waitingFailureReason,
	connectionFailed,
	hostUnrecheable,
	invalidXML,
	invalidAuth
	
}
